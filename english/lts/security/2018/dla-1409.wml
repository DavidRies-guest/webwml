<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7651">CVE-2017-7651</a>
      <p>fix to avoid extraordinary memory consumption by crafted
      CONNECT packet from unauthenticated client</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7652">CVE-2017-7652</a>
      <p>in case all sockets/file descriptors are exhausted, this is a
      fix to avoid default config values after reloading configuration
      by SIGHUP signal</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.4-2+deb8u2.</p>

<p>We recommend that you upgrade your mosquitto packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1409.data"
# $Id: $
