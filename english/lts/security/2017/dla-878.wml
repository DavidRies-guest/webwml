<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6298">CVE-2017-6298</a>

      <p>Null Pointer Deref / calloc return value not checked</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6299">CVE-2017-6299</a>

      <p>Infinite Loop / DoS in the TNEFFillMapi function in lib/ytnef.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6300">CVE-2017-6300</a>

      <p>Buffer Overflow in version field in lib/tnef-types.h</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6301">CVE-2017-6301</a>

      <p>Out of Bounds Reads</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6302">CVE-2017-6302</a>

      <p>Integer Overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6303">CVE-2017-6303</a>

      <p>Invalid Write and Integer Overflow</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6304">CVE-2017-6304</a>

      <p>Out of Bounds read</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6305">CVE-2017-6305</a>

      <p>Out of Bounds read and write</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6801">CVE-2017-6801</a>

      <p>Out-of-bounds access with fields of Size 0 in TNEFParse() in libytnef</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-6802">CVE-2017-6802</a>

      <p>Heap-based buffer over-read on incoming Compressed RTF Streams,
      related to DecompressRTF() in libytnef</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.5-4+deb7u1.</p>

<p>We recommend that you upgrade your libytnef packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-878.data"
# $Id: $
