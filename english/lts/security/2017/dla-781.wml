<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities were discovered in Asterisk, an Open
Source PBX and telephony toolkit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2287">CVE-2014-2287</a>

    <p>channels/chan_sip.c in Asterisk when chan_sip has a certain
    configuration, allows remote authenticated users to cause a denial
    of service (channel and file descriptor consumption) via an INVITE
    request with a (1) Session-Expires or (2) Min-SE header with a
    malformed or invalid value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7551">CVE-2016-7551</a>

    <p>The overlap dialing feature in chan_sip allows chan_sip to report
    to a device that the number that has been dialed is incomplete and
    more digits are required. If this functionality is used with a
    device that has performed username/password authentication RTP
    resources are leaked. This occurs because the code fails to release
    the old RTP resources before allocating new ones in this scenario.
    If all resources are used then RTP port exhaustion will occur and
    no RTP sessions are able to be set up.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1:1.8.13.1~dfsg1-3+deb7u5.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-781.data"
# $Id: $
