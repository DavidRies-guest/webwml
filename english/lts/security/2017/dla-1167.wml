<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability was found in ruby-yajl, an interface to Yajl, a JSON
stream-based parser library. When a crafted JSON file is supplied to
Yajl::Parser.new.parse, the whole ruby process crashes with a SIGABRT
in the yajl_string_decode function in yajl_encode.c. This may result
in a denial of service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.0-2+deb7u1.</p>

<p>We recommend that you upgrade your ruby-yajl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1167.data"
# $Id: $
