#use wml::debian::translation-check translation="1d1f8d159bd57a26b5a8603a6dfc4a1937981b1c"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en Samba, un servidor de ficheros,
impresión y acceso («login») SMB/CIFS para Unix. El proyecto «Vulnerabilidades y exposiciones comunes» («Common Vulnerabilities and
Exposures») identifica los problemas siguientes:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

    <p>Florian Stuelpner descubrió que Samba es vulnerable a
    recursión de consulta infinita («infinite query recursion») provocada por bucles de CNAME, dando lugar a
    denegación de servicio.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-14629.html">\
    https://www.samba.org/samba/security/CVE-2018-14629.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16841">CVE-2018-16841</a>

    <p>Alex MacCuish descubrió que un usuario con un certificado o tarjeta
    inteligente válidos puede provocar la caída del KDC de Samba AD DC cuando está configurado para aceptar
    autenticación con tarjeta inteligente («smart-card authentication»).</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16841.html">\
    https://www.samba.org/samba/security/CVE-2018-16841.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

    <p>Garming Sam, del equipo Samba y de Catalyst, descubrió una vulnerabilidad
    de desreferencia de puntero NULL en el servidor Samba AD DC LDAP que permite que
    un usuario con capacidad para leer más de 256MB de entradas LDAP provoque la caída del servidor
    LDAP de Samba AD DC.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16851.html">\
    https://www.samba.org/samba/security/CVE-2018-16851.html</a></p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 2:4.5.12+dfsg-2+deb9u4.</p>

<p>Le recomendamos que actualice los paquetes de samba.</p>

<p>Para información detallada sobre el estado de seguridad de samba, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4345.data"
