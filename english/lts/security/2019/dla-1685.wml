<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Drupal core uses the third-party PEAR Archive_Tar library. This
library has released a security update which impacts some Drupal
configurations. Refer to <a href="https://security-tracker.debian.org/tracker/CVE-2018-1000888">CVE-2018-1000888</a> for details. Also a possible
regression caused by <a href="https://security-tracker.debian.org/tracker/CVE-2019-6339">CVE-2019-6339</a> is fixed.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
7.32-1+deb8u15.</p>

<p>We recommend that you upgrade your drupal7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1685.data"
# $Id: $
