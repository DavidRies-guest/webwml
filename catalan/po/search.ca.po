# search webwml Catalan template.
# Copyright (C) 2002 Free Software Foundation, Inc.
# Jordi Mallach <jordi@debian.org>, 2002.
# Guillem Jover <guillem@debian.org>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml\n"
"PO-Revision-Date: 2018-07-12 00:59+0200\n"
"Last-Translator: Guillem Jover <guillem@debian.org>\n"
"Language-Team: Catalan <debian-l10n-catalan@lists.debian.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/searchtmpl/search.def:6
msgid "Search for"
msgstr "Cercar"

#: ../../english/searchtmpl/search.def:10
msgid "Results per page"
msgstr "Resultats per pàgina"

#: ../../english/searchtmpl/search.def:14
msgid "Output format"
msgstr "Format de la sortida"

#: ../../english/searchtmpl/search.def:18
msgid "Long"
msgstr "Llarg"

#: ../../english/searchtmpl/search.def:22
msgid "Short"
msgstr "Curt"

#: ../../english/searchtmpl/search.def:26
msgid "URL"
msgstr "Adreça"

#: ../../english/searchtmpl/search.def:30
msgid "Default query type"
msgstr "Tipus de consulta"

#: ../../english/searchtmpl/search.def:34
msgid "All Words"
msgstr "Totes les paraules"

#: ../../english/searchtmpl/search.def:38
msgid "Any Words"
msgstr "Qualsevol de les paraules"

#: ../../english/searchtmpl/search.def:42
msgid "Search through"
msgstr "Cercar en"

#: ../../english/searchtmpl/search.def:46
msgid "Entire site"
msgstr "Servidor sencer"

#: ../../english/searchtmpl/search.def:50
msgid "Language"
msgstr "Llengua"

#: ../../english/searchtmpl/search.def:54
msgid "Any"
msgstr "Qualsevol"

#: ../../english/searchtmpl/search.def:58
msgid "Search results"
msgstr "Resultats de la cerca"

#: ../../english/searchtmpl/search.def:65
msgid ""
"Displaying documents \\$(first)-\\$(last) of total <B>\\$(total)</B> found."
msgstr ""
"S'estan mostrant els documents \\$(first)-\\$(last) d'un total de <b>\\"
"$(total)</b> trobats."

#: ../../english/searchtmpl/search.def:69
msgid ""
"Sorry, but search returned no results. <P>Some pages may be available in "
"English only, you may want to try searching again and set the Language to "
"Any."
msgstr ""
"Ho sentim, però la cerca no ha tornat cap resultat. <P>Algunes pàgines poden "
"estar disponibles en anglès únicament, potser voleu tornar a intentar-ho "
"establint la Llengua a Qualsevol."

#: ../../english/searchtmpl/search.def:73
msgid "An error occured!"
msgstr "S'ha produït un error!"

#: ../../english/searchtmpl/search.def:77
msgid "You should give at least one word to search for."
msgstr "Deuríeu introduir al menys una paraula a cercar"

#: ../../english/searchtmpl/search.def:81
msgid "Powered by"
msgstr "Impulsat per"

#: ../../english/searchtmpl/search.def:85
msgid "Next"
msgstr "Següent"

#: ../../english/searchtmpl/search.def:89
msgid "Prev"
msgstr "Anterior"
