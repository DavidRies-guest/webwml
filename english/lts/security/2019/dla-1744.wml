<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2019a. Notable
changes are:</p>

<ul>
 <li>Palestine started DST on 2019-03-30, instead of 2019-03-23
   as previously predicted.</li>
 <li>Metlakatla ended its observance of Pacific standard time, rejoining
   Alaska Time, on 2019-01-20 at 02:00.</li>
</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2019a-0+deb8u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1744.data"
# $Id: $
