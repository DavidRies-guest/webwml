<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Clement Lecigne discovered a use-after-free issue in chromium's file
reader implementation.  A maliciously crafted file could be used to
remotely execute arbitrary code because of this problem.</p>

<p>This update also fixes a regression introduced in a previous update.  The
browser would always crash when launched in remote debugging mode.</p>

<p>For the stable distribution (stretch), this problem has been fixed in
version 72.0.3626.122-1~deb9u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4404.data"
# $Id: $
