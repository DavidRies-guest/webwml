<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Brief introduction </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9911">CVE-2014-9911</a>

    <p>Michele Spagnuolo discovered a buffer overflow vulnerability which
    might allow remote attackers to cause a denial of service or possibly
    execute arbitrary code via crafted text.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7415">CVE-2016-7415</a>

    <p>A stack-based buffer overflow might allow an attacker with control of
    the locale string to perform a denial of service and, possibly,
    execute arbitrary code.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.8.1.1-12+deb7u6.</p>

<p>We recommend that you upgrade your icu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-744.data"
# $Id: $
