<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Go before 1.8.4 and 1.9.x before 1.9.1 allows <q>go get</q> remote command
execution. Using custom domains, it is possible to arrange things so
that example.com/pkg1 points to a Subversion repository but
example.com/pkg1/pkg2 points to a Git repository. If the Subversion
repository includes a Git checkout in its pkg2 directory and some
other work is done to ensure the proper ordering of operations, <q>go
get</q> can be tricked into reusing this Git checkout for the fetch of
code from pkg2. If the Subversion repository's Git checkout has
malicious commands in .git/hooks/, they will execute on the system
running "go get."</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.0.2-1.1+deb7u2.</p>

<p>We recommend that you upgrade your golang packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1148.data"
# $Id: $
