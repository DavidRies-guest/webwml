<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Pysaml2, a Python implementation of the Security Assertion Markup
Language, would accept any password when run with Python optimizations
enabled. This allows attackers to log in as any user without knowing
their password.</p>

<p>For Debian 8 <q>Jessie</q>, this issue has been fixed in version
2.0.0-1+deb8u2.</p>

<p>We recommend that you upgrade your python-pysaml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1410.data"
# $Id: $
